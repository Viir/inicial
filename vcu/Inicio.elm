module Inicio exposing 
    ( main
    )

-- IMPORTS ###################################################################

import Browser
import Html exposing (Html, text, div, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)

-- TYPES ###################################################################

type alias Model =
    { seccion : Seccion
    }

type alias Flags =
    {}

type Msg
    = NoOp
    | CambiarSeccion Seccion

type Seccion
    = Inicio
    | Contacto
    | Galeria
    | Musica Int GeneroMusical

type GeneroMusical
    = Rock
    | Pop

-- CONSTANTS ###################################################################

-- INITIAL MODELS ###################################################################

initialModel : Flags -> ( Model, Cmd Msg ) 
initialModel flags =
    ( { seccion = Inicio
        }
    , Cmd.none
    )

-- UPDATE ###################################################################

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )
        
        CambiarSeccion newSeccion ->
            ( { seccion = newSeccion
                }
            , Cmd.none
            )

-- UPDATES ###################################################################

-- CALLBACKS ###################################################################

-- JSON DECODERS ###################################################################

-- METHODS ###################################################################
{- 
lista=
[]
e :: []
e :: restoElementos
e :: e2 :: restoElementos
-}

extraerValores : List Seccion -> String
extraerValores lista =
    case lista of
        [] ->
            "es vacía"
    
        e :: [] -> -- [Inicio] == Inicio :: []
            "sólo tiene un elemento y es:" ++ (seccion2str e)

        e :: e2 :: [] -> -- [Inicio, Galeria] == Inicio :: Galeria :: []
            "tiene 2 elementos y son:" ++ (seccion2str e) ++ (seccion2str e2)

        _ -> -- [Inicio, Galeria, ...] == Inicio :: Galeria :: []
            "tiene 3 o más elementos"

extraerDeMaybe : Maybe Seccion -> String
extraerDeMaybe podriaSeccion =
    case podriaSeccion of
        Just x ->
            "el elemento es:" ++ (seccion2str x)

        Nothing ->
            "No tiene valor"

extraerDeTuplas : (Seccion, Seccion) -> String
extraerDeTuplas tupla =
    case tupla of
        (Inicio, Inicio) ->
            "está en su sección favorita:"

        (Galeria, Galeria) ->
            "está en su sección favorita:"

        (Galeria, Contacto) ->
            "la siguiente sección es tu favorita"

        (Contacto, _) ->
            "está en el contacto"

        (actual, _) ->
            "la sección actual es: " ++ (seccion2str actual)

extraerNum : Int -> String
extraerNum num =
    case num of
        1 ->
            "está en su sección 1"

        2 ->
            "está en su sección 2"

        x ->
            "está en su sección" ++ (String.fromInt x)

extraerGenero : Seccion -> Maybe GeneroMusical
extraerGenero seccion =
    case seccion of
        Musica _ gen ->
            Just gen

        _ ->
            Nothing

comparacionX : Int -> Int -> String
comparacionX num1 num2 =
    case (num1 == num2, num1 > num2) of
        (False, True) ->
            "El num1 > el num2"

        (True, _) ->
            "los nums son iguales"

        (False, False) ->
            "El num1 < el num2"

extraerDeTuplas2 : (Seccion, Seccion) -> String
extraerDeTuplas2 (a, b) =
    "la tupla es:" ++ (seccion2str a) ++ (seccion2str b)

seccion2str : Seccion -> String
seccion2str sec =
    case sec of
        Inicio ->
            "Inicio"
    
        Contacto ->
            "Contacto"

        Galeria ->
            "Galería"

        Musica _ Pop ->
            "Musica Pop"

        Musica _ Rock ->
            "Musica Rock"

-- VIEWS ###################################################################

view : Model -> Html Msg
view model =
    let
        strSeccion = seccion2str model.seccion
    in
        div []
            [ button [ onClick <| CambiarSeccion Inicio ] [ text "Inicio" ]
            , button [ onClick <| CambiarSeccion Contacto ] [ text "Contacto" ]
            , button [ onClick <| CambiarSeccion Galeria ] [ text "Galeria" ]
            , div []
                [ text <| "Te encuentras en la sección: " ++ strSeccion
                ]
            ]

-- OUT Ports ###################################################################

-- IN Ports ###################################################################

-- SUBSCRIPTIONS ###################################################################

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [
        ]

-- END ###################################################################

main : Program Flags Model Msg
main =
    Browser.element
        { init = initialModel
        , view = view
        , update = update
        , subscriptions = subscriptions
        }